import pygame
from pygame.sprite import Group

from Bullet import Bullet


class Ship():
    def __init__(self, screen):
        """Initialize the ship and set its starting position."""

        self.screen = screen

        # Load the ship image and get its rect.
        self.image = pygame.image.load('images/ship.png')

        # Start each new ship at the bottom center of the screen.
        self.rect = self.image.get_rect()
        self.rect.centerx = screen.get_rect().centerx
        self.rect.bottom = screen.get_rect().bottom

        self.movingRight = False
        self.movingLeft = False
        self.bullet = Group()


    def moveRight(self, movement):
        self.movingRight = movement
        self.movingLeft = False

    def moveLeft(self, movement):
        self.movingRight = False
        self.movingLeft = movement

    def shootBullet(self):
        new_bullet = Bullet(screen=self.screen, ship=self)
        self.bullet.add(new_bullet)

    def update(self):
        for bullet in self.bullet.copy():  # .copy() is used in order to use bullets.remove above
            if bullet.rect.bottom <= 0:
                self.bullet.remove(bullet)

        for bullet in self.bullet.sprites():
            bullet.update()

        if self.movingRight:
            if self.rect.centerx < self.screen.get_rect().width:
                self.rect.centerx += 1
        elif self.movingLeft:
            if self.rect.centerx > 0:
                self.rect.centerx -= 1

    def draw(self):
        """Draw the ship at its current location."""
        self.screen.blit(self.image, self.rect)  # draws the Ship image into the screen
        for bullet in self.bullet.sprites():
            bullet.draw()
